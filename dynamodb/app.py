import json

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute

class ModeloVendedor(Model):
    class Meta:
        table_vendedor = 'sam-app-myDynamoDBTable-1JCNFBP2SK67G' #'sam-app-myDynamoDBTable-14V97OIOPTKT4'
        region = 'us-west-1'
    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)   
    name_vendedor = UnicodeAttribute(default='')
    email_vendedor = UnicodeAttribute(default='')


class ModeloComprador(Model):
    class Meta:
        table_comprador = 'sam-app-myDynamoDBTable-14V97OIOPTKT4'
        region = 'us-west-1'
    PK = UnicodeAttribute(hash_key=True)
    SK = UnicodeAttribute(range_key=True)   
    name_comprador = UnicodeAttribute(default='')
    email_comprador  = UnicodeAttribute(default='')
    #email = UnicodeAttribute(hash_key=True)
   


def lambda_handler(event, context):
    

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "Felipe",
            # "location": ip.text.replace("\n", "")
        }),
    }
